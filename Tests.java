
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import java.util.List;
import java.util.stream.Collectors;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    String URL = "http://localhost:5000/api/books";
    int ID = 3;
    int IDSearch = 1;
    int IDWrong = 100;

    @Test
    @Order(1)
    @DisplayName("Check that the resulting list of books is not empty.")

    public void test1_1() {
    List<BookDate> books = given()
            .baseUri(URL)
            .contentType(ContentType.JSON)
            .when().get()
            .then().log().all()
            .extract().body()
            .jsonPath().getList("books", BookDate.class);
            assertNotNull(books);
}
    @Test
    @Order(2)
    @DisplayName("Check that books have a correctly filled name field.")
    public void test1_2() {
        List<BookDate> books = given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().log().all()
                .extract().body()
                .jsonPath().getList("books", BookDate.class);
        List<String> name = books.stream().map(BookDate::getName).collect(Collectors.toList());
        for (int i = 0; i < name.size(); i++) {
            Assertions.assertNotNull(name.get(i));
        }
    }
    @Test
    @Order(3)
    @DisplayName("Check that all books have unique IDs.")

    public void test1_3() {

        List<BookDate> books = given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().log().all()
                .extract().body()
                .jsonPath().getList("books", BookDate.class);
        List<Integer> ids = books.stream().map(x -> x.getId()).collect(Collectors.toList());
        for (int i = 0; i < ids.size(); i++) {
            for (int k = i + 1; k < ids.size(); k++) {
                Assertions.assertTrue(ids.get(i) != null && !(ids.get(i).equals(ids.get(k))));
            }
        }
    }
    @Test
    @Order(4)
    @DisplayName("Check if the book ID matches the requested ID.")

    public void test2_1() {
                given()
                .baseUri(URL+"/"+IDSearch)
                .contentType(ContentType.JSON)
                .when().get()
                .then().log().all()
                .statusCode(200)
                .body("book.id", equalTo(IDSearch));
    }
    @Test
    @Order(5)
    @DisplayName("Check that when entering the identifier of a non-existent book( 404 error")
    public void test2_2() {
                 given()
                .baseUri(URL+"/"+IDWrong)
                .when().get()
                .then()
                .statusCode(404);
    }
    @Test
    @Order(6)
    @DisplayName("Check that a new book is added to the list.")
    public void test3_1() {

        JSONObject date = new JSONObject();
        date.put("author", "Александр Дюма");
        date.put("isElectronicBook", true);
        date.put("name", "Три мушкетёра");
        date.put("year", 2015);

                given()
                .contentType(ContentType.JSON).
                body(date.toString()).
                when()
                .post(URL)
                .then()
                .statusCode(201);
    }

    @Test
    @Order(7)
    @DisplayName("Check that the new book has correctly filled fields name,author,isElectronicBook and year.")
    public void test3_2() {
        List<BookDate> books = given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().log().all()
                .extract().body()
                .jsonPath().getList("books", BookDate.class);
        List<Integer> ids = books.stream().map(x -> x.getId()).collect(Collectors.toList());

                given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().statusCode(200)
                .body("books["+(ids.size()-1)+"].id", equalTo(ID))
                .body("books["+(ids.size()-1)+"].name", equalTo("Три мушкетёра"))
                .body("books["+(ids.size()-1)+"].author", equalTo("Александр Дюма"))
                .body("books["+(ids.size()-1)+"].isElectronicBook", equalTo(true));
    }

    @Test
    @Order(8)
    @DisplayName("Check that the new book has a unique ID.")
    public void test3_3() {
        List<BookDate> books = given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().log().all()
                .extract().body()
                .jsonPath().getList("books", BookDate.class);

        List<Integer> ids = books.stream().map(x -> x.getId()).collect(Collectors.toList());
        for (int i = 0; i < ids.size(); i++) {
            for (int k = i + 1; k < ids.size(); k++) {
                Assertions.assertTrue(ids.get(i) != null && !(ids.get(i).equals(ids.get(k))));
            }
        }
    }

    @Test
    @Order(9)
    @DisplayName("Check that the book information is updated successfully (all fields except id)")
    public void test4_1() {
        JSONObject date = new JSONObject();
        date.put("author", "Жюль Верн");
        date.put("isElectronicBook", false);
        date.put("name", "Таинственный остров");
        date.put("year", 2000);

                given()
                .contentType(ContentType.JSON)
                .body(date.toString())
                .when().put(URL + "/" + ID)
                .then().statusCode(200);
    }

    @Test
    @Order(10)
    @DisplayName("Check that the updated book has correctly filled fields name,author,isElectronicBook and year.")
    public void test4_2() {
                given()
                .baseUri(URL)
                .contentType(ContentType.JSON)
                .when().get()
                .then().statusCode(200)
                .body("books["+ID+"].name", equalTo("Таинственный остров"))
                .body("books["+ID+"].author", equalTo("Жюль Верн"))
                .body("books["+ID+"].isElectronicBook", equalTo(false));

    }

    @Test
    @Order(11)
    @DisplayName("Verify that the requested workbook is being updated and not a new one is being created.")
    public void test4_3() {
                given().baseUri(URL).contentType(ContentType.JSON)
                .when().get()
                .then().statusCode(200)
                .body("books["+(ID-1)+"].id", equalTo(ID))
                .extract().jsonPath().getList("books", BookDate.class);
    }
    @Test
    @Order(12)
    @DisplayName("Check that it is not possible to delete a non-existent workbook (error 404)")
    public void test5_1() {
                         given()
                        .baseUri(URL+"/"+ IDWrong).contentType(ContentType.JSON)
                        .when().delete()
                        .then()
                        .statusCode(404);

    }
    @Test
    @Order(13)
    @DisplayName("Verify that the requested book is successfully deleted")
    public void test5_2() {

                given()
                .baseUri(URL + "/" + ID)
                .when()
                .delete()
                .then()
                .statusCode(200);
    }

    @Test
    @Order(14)
    @DisplayName("Check that it is impossible to get information about the deleted book (error 404).")
    public void test5_3() {
                 given()
                .baseUri(URL + "/" + ID)
                .when()
                .delete()
                .then()
                .statusCode(404);
    }
    }


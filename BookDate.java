
public class BookDate {
    private int id;
    private String name;
    private String author;
    private int year;
    private boolean isElectronicBook;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getYear(){
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public boolean getIsElectronicBook() {
        return isElectronicBook;
    }

    public void setElectronicBook(boolean isElectronicBook) {
        this.isElectronicBook = isElectronicBook;
    }
}
